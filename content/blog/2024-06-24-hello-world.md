+++
title = "Hello, world!"
+++

Hello world! Welcome to my blog. I will be posting stuff I feel like writing
about here.

## A little about me

I am a computer nerd who writes code & messes around with Linux for fun. I am
currently daily-driving [NixOS](https://nixos.org). I also like music (I play
bass guitar, and have been learning acoustic guitar).

You may find my contact information on the [homepage](/) of this website.
There is also an Atom feed available in the navigation bar.

I hope you enjoy reading my silly ramblings :)
